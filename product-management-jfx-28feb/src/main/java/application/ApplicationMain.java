package application;

import db_operation.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import show_option.ShowOption;
import stage_master.StageMaster;

public class ApplicationMain extends Application {

	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
	}
	
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		System.out.println("Application start");
		new ShowOption().Show();
		System.out.println("Application stop");
	}
}
