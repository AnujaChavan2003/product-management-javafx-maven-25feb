package search_product;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import show_option.ShowOption;

public class SearchProductController {

 static Statement stmt;
 static Connection con;
	@FXML
	private TextField searchName;
	@FXML
	private Button SearchTextBox;
	@FXML
	private Button back;
	@FXML
	private Button next;
	
	public void SearchProduct(ActionEvent event) throws Exception {
    System.out.println("Search = "+searchName.getText()); 
		
		String query ="select * from Product where Name =\""+ searchName.getText()+"\";";
		System.out.println(query);
		ResultSet resultSet = DbUtil.executeQueryGetResult(query);
		while(resultSet.next()) {
			if(searchName.getText().contentEquals(resultSet.getString(1))) {
				System.out.println("Product found");
			}
			else {
				System.out.println("Product Not Found");
			}
		}
		
		db_operation.DbUtil.executeQuery(query);
		System.out.println("Event occur search controller "+event.getEventType().getName());

		new ShowOption().Show();
	}
}
