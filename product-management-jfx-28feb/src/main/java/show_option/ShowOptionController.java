package show_option;

import add_product.AddProduct;
import delete_product.DeleteProduct;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import search_product.SearchProduct;
import update_product.UpdateProduct;

public class ShowOptionController {

	@FXML
	private Button add;
	@FXML
	private Button delete;
	@FXML
	private Button update;
	@FXML
	private Button search;
	@FXML
	private Button Exit;
	
	public void add(ActionEvent event) {
		new AddProduct().Show();
	}
	public void delete(ActionEvent event) {
		new DeleteProduct().Show();
	}
	public void update(ActionEvent event) {
		new UpdateProduct().Show();
	}
	public void search(ActionEvent event) {
		new SearchProduct().Show();
	}
	public void exit(ActionEvent event) {
		System.out.println("Exit Application");
	}
	
	
}
